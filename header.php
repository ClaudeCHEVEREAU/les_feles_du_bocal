<?php
/**
 * The template for displaying the header.
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    
    <?php wp_body_open(); ?>
<header>
    <?php if ( function_exists( 'the_custom_logo' ) ) {
        ?> <a href ="<?php echo get_home_url( ); ?>">
    <?php the_custom_logo(); ?>
    </a>
<?php } ?>

    <?php wp_nav_menu( 'nav_menu' ); ?>
</header>
<?php
