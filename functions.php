<!-- <?php

//Ajouter la prise en charge des images mises en avant
add_theme_support( 'post-thumbnails' );

//Ajouter automatiquement le titre du site dans l'entète du site
add_theme_support( 'title-tag' );


if(!function_exists("registerMyPostTypes")) {
    function registerMyPostTypes() {
    $labelsBocal = array(
        "name"                  => __("bocal", "enssop"),
        "singular_name"         => __("bocal", "enssop"),
        "menu_name"             => __("Bocal", "enssop"),
        "all_items"             => __("Tous les bocaux", "enssop"),
        "add_new"               => __("Ajouter un bocal", "enssop"),
        "add_new_item"          => __("Ajouter un nouveau bocal", "enssop"),
        "edit_item"             => __("Ajouter un bocal", "enssop"),
        "new_item"              => __("Nouveau bocal", "enssop"),
        "view_item"             => __("Voir le bocal", "enssop"),
        "view_items"            => __("Voir les bocaux", "enssop"),
        "search_items"          => __("Chercher un bocal", "enssop"),
        "not_found"             => __("Pas de bocal trouvé", "enssop"),
        "not_found_in_trash"    => __("Pas de bocal trouvé dans la corbeille", "enssop"),
        "featured_image"        => __("Image mise en avant pour ce bocal", "enssop"),
        "set_featured_image"    => __("Définir une image mise en avant pour ce bocal", "enssop"),
        "remove_featured_image" => __("Supprimer l'image mise en avant pour ce bocal", "enssop"),
        "use_featured_image"    => __("Utiliser comme image mise en avant pour ce bocal", "enssop"),
        "archives"              => __("Type de bocal", "enssop"),
        "insert_into_item"      => __("Ajouter au bocal", "enssop"),
        "uploaded_to_this_item" => __("Ajouter au bocal", "enssop"),
        "filter_items_list"     => __("Filtrer la liste des bocaux", "enssop"),
        "items_list_navigation" => __("Naviguer dans la liste des bocaux", "enssop"),
        "items_list"            => __("Liste des bocaux", "enssop"),
        "attributes"            => __("Paramètres du bocal", "enssop"),
        "name_admin_bar"        => __("bocal", "enssop"),
    );

    $argsBocal = array(
        "label"                 => __("bocaux", "enssop"),
        "labels"                => $labelsBocal,
        "description"           => __("bocaux du site", "enssop"),
        "public"                => true,
        "publicly_queryable"    => true,
        "show_ui"               => true,
        "delete_with_user"      => false,
        "show_in_rest"          => false,
        "has_archive"           => true,
        "show_in_menu"          => true,
        "show_in_nav_menu"      => true,
        "menu_position"         => 4,
        "exclude_from_search"   => false,
        "capability_type"       => "post",
        "map_meta_cap"          => true,
        "hierarchical"          => false,
        "rewrite"               => array("slug" => "bocal", "with_front" => true),
        "query_var"             => 'bocal',
        "menu_icon"             => "dashicons-smiley",
        "supports"              => array("title", "editor", "author", "thumbnail", "excerpt", "trackbacks",
                                         "custom-fields", "comments", "revisions", "page-attributes",
                                         "post-formats"),
    );
    register_post_type( 'bocal', $argsBocal );
    }
}
add_action('init', 'registerMyPostTypes');

/**
 * strat register taxonomy
 * 
 * strat taxo formules
*/
if(!defined('enssop')){
function formules_taxo(){

    $labelsFormules = array(
        'name'              => __( 'Formules', 'enssop' ),
        'singular_name'     => __( 'Formule', 'enssop' ),
        'search_items'      => __( 'Rechercher une formule', 'enssop' ),
        'all_items'         => __( 'Toutes les formules', 'enssop' ),
        'parent_item'       => __( 'Formule parente', 'enssop' ),
        'parent_item_colon' => __( 'Formule parente :', 'enssop' ),
        'edit_item'         => __( 'Modifier la formule', 'enssop' ),
        'update_item'       => __( 'Modifier la formule', 'enssop' ),
        'add_new_item'      => __( 'Ajouter une formule' , 'enssop'),
        'new_item_name'     => __( 'Nouvelle formule', 'enssop' ),
        'menu_name'         => __( 'Formules', 'enssop' ),
        );
        
    $argsFormules   = array(
        'hierarchical'  => true,
        'labels'        => $labelsFormules,
        'show_ui'       => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite' => array( 'slug' => 'formules'),
        );
        register_taxonomy( 'formules', array('bocal'), $argsFormules); 
    }
}
add_action('init', 'formules_taxo');
    //end taxo formules
  
    
    if(!function_exists("addstylesheets")) {
    function addstylesheets(){
    wp_enqueue_style( "font-awesome", "https://use.fontawesome.com/releases/v5.6.3/css/all.css" );
    wp_enqueue_style( "bootstrap", get_stylesheet_directory_uri()."/css/bootstrap.min.css" );
    wp_enqueue_style( "my-stylesheet", get_stylesheet_directory_uri().'/style.css' );
    wp_enqueue_script( "jquery","https://code.jquery.com/jquery-3.2.1.slim.min.js" );
    wp_enqueue_script( "bootstrap-js" , get_stylesheet_directory_uri().'/js/bootstrap.min.js' );
    }
    }
    add_action( 'wp_enqueue_scripts', 'addstylesheets');
    
    register_nav_menu("nav_menu",__('Menu principal', 'enssop'));
    