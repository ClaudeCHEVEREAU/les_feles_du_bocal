<?php

get_header();

?>

    <main class='archive-bocal'>

        <?php

        $terms = get_terms( array(
                                'taxonomy' => 'formules',
                                'hide_empty' => false,
                            ) );

        foreach ($terms as $term){
//recuperates the posts bocal
            $args = array(
                'post_type' => 'bocal',
                'numberposts' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'formules',
                        'field'    => 'slug',
                        'terms'    => $term->slug,
                    ),
                ),
            );
            ?>
            <h2><?php echo $term->name; ?></h2>
            <?php
            $query = new WP_Query($args);

//renders the template
            if ($query->have_posts()) {
                while ( $query->have_posts() ) {
                    $query->the_post();
                    ?>

                    <article class='bocal'>
                     <!--   <img src='<?php //the_post_thumbnail_url(); ?>' >  -->
                     <img src='/les-feles-du-bocal/wordpress/wp-content/uploads/2020/07/Epices.jpg'>
                        <h3><?php the_title() ?></h3>
                        <a href='<?php the_permalink();?>'>Voir le produit</a>
                    </article>
                    <?php
                }
            }
        }

        ?>

    </main>

<?php get_footer();
