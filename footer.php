<?php
?>
<footer>
    <!--div logo and social media-->
    <div>
        <?php the_custom_logo(); ?>
        <p>Suivez-nous sur :</p>
        <a href="https://www.instagram.com/lesfelesdubocal/" target="_blank"></a>
        <a href="https://www.facebook.com/felesdubocal/" target="_blank"></a>
    </div>
    <!--div contact information-->
    <div>
        <p class="footercontact"
            02 43 78 25 11 <br>
            alexandre@les-feles-du-bocal.bio <br>
            198 rue Nationale <br>
            Place Washington <br>
            72000 Le Mans <br>
            <a href="#">Mentions légales</a>
        </p>
    </div>
    <!--div menu-->
    <div>
        <?php wp_nav_menu( 'nav_menu' ); ?>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>